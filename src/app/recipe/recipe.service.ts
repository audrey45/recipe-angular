import { ShoppingListService } from './../shopping-list/shopping-list.service';
import { Ingredient } from './../shared/ingredient.model';
import { EventEmitter, Injectable } from '@angular/core';
import { Recipe } from './recipe.model';

@Injectable()
export class RecipeService {

  private recipeList: Recipe[] = [
    new Recipe('Fava Bean and Carrot Salad', 'This quick and easy salad takes some of spring\'s best produce',
      'http://www.seriouseats.com/recipes/assets_c/2016/05/20160503-fava-carrot-ricotta-salad-recipe-1-thumb-1500xauto-431710.jpg', [
        new Ingredient('Slices hearty toast', 4),
        new Ingredient('Small carrots', 2),
        new Ingredient('Fresh fava bean pods', 1.5)

      ]),
    new Recipe('Texas Pulled Pork', 'Pulled pork that is served on a buttered and toasted roll',
      'http://images.media-allrecipes.com/userphotos/250x250/870874.jpg', [
        new Ingredient('Pork Shoulder Roast', 1),
        new Ingredient('Barbeque Saucet(cup)', 1),
        new Ingredient('Buns', 2)
      ])
  ];

  constructor(private shoppingListService: ShoppingListService) { }

  getRecipeList() {
    return this.recipeList.slice();
  }


  onToShoppingList(ingredients: Ingredient[]) {
    this.shoppingListService.ingredients.push(...ingredients);
  }

  getRecipe(index: number) {
    return this.recipeList.slice()[index];
  }

}
