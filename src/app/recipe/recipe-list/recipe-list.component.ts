import { Component, OnInit, Output, EventEmitter } from '@angular/core';

import { RecipeService } from './../recipe.service';
import { Recipe } from '../recipe.model';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-recipe-list',
  templateUrl: './recipe-list.component.html',
  styleUrls: ['./recipe-list.component.css']
})
export class RecipeListComponent implements OnInit {

  @Output() recipeSelectedList = new EventEmitter<Recipe>();
  recipeList: Recipe[];

  constructor(private recipeService: RecipeService,
    private router: Router,
    private route: ActivatedRoute) { }

  ngOnInit() {
    this.recipeList = this.recipeService.getRecipeList();
  }

  onRecipeMainSelect(recipe: Recipe) {
    this.recipeSelectedList.emit(recipe);
  }

  onNewRecipe() {

    this.router.navigate(['new'], { relativeTo: this.route });

  }

}
