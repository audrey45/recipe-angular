import { Component, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})

export class HeaderComponent {
  @Output() navigator = new EventEmitter<string>();

  onNavigate(menu: string) {
    this.navigator.emit(menu);
  }

}
