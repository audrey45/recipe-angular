import { ShoppingListService } from './shopping-list/shopping-list.service';
import { RecipeService } from './recipe/recipe.service';
import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [RecipeService,ShoppingListService]
})
export class AppComponent {
  title = 'app works!';

  // loadedMenu = 'recipe';

  // onNavigate(menu: string) {
  //   this.loadedMenu = menu;
  // }

}
