import { Directive, HostListener, HostBinding } from '@angular/core';

@Directive({
  selector: '[appDropdown]'
})
export class DropdownDirective {

  @HostBinding('class.open') classOpen = false;
  constructor() { }



  @HostListener('click') mouseclick(eventdata: Event) {
    this.classOpen = !this.classOpen;
  }


}
